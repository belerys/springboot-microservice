package net.javaguides.departementservice.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import net.javaguides.departementservice.dto.DepartmentDTO;
import net.javaguides.departementservice.service.DepartmentService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@Tag(
    name ="Department Service - DepartmentController",
    description = "Department Controller exposes REST APIs for Departmnet-Service"
)
@RestController
@AllArgsConstructor
@RequestMapping("api/departments")
public class DepartmentController {

    private DepartmentService departmentService;

    //Build save department REST API
    @Operation(
        summary = "Save Department REST API",
        description = "Save Department REST API is used to save department object in a database"
    )
    @ApiResponse(
        responseCode = "201",
        description = "HTTP Status 201 CREATED"
    )
    @PostMapping
    public ResponseEntity<DepartmentDTO> saveDepartment(@RequestBody DepartmentDTO departmentDTO) {
        DepartmentDTO savedDepartment = departmentService.saveDepartment(departmentDTO);
        return new ResponseEntity<>(savedDepartment, HttpStatus.CREATED);
    }

    //Build get department REST API
    @Operation(
        summary = "get Department REST API",
        description = "Get Department REST API is used to get department object from a database"
    )
    @ApiResponse(
        responseCode = "200",
        description = "HTTP Status 200 SUCCESS"
    )
    @GetMapping("{department-code}")
    public ResponseEntity<DepartmentDTO> getDepartmentByCode(@PathVariable("department-code") String code) {
        DepartmentDTO departmentDTO = departmentService.getDepartmentByCode(code);

        return new ResponseEntity<>(departmentDTO, HttpStatus.OK);
    }
}

package net.javaguides.departementservice.mapper;

import net.javaguides.departementservice.dto.DepartmentDTO;
import net.javaguides.departementservice.entity.Department;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.WARN, uses = { MappingUtils.class })
public interface DepartmentMapper {

    DepartmentDTO toDto(Department department);

    Department toEntity(DepartmentDTO departmentDTO);
}


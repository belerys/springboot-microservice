package net.javaguides.departementservice.service;

import lombok.AllArgsConstructor;
import net.javaguides.departementservice.dto.DepartmentDTO;
import net.javaguides.departementservice.entity.Department;
import net.javaguides.departementservice.mapper.DepartmentMapper;
import net.javaguides.departementservice.repository.DepartmentRepository;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class DepartmentServiceImpl implements DepartmentService {


    private DepartmentRepository departmentRepository;

    private DepartmentMapper departmentMapper;

    @Override
    public DepartmentDTO saveDepartment(DepartmentDTO departmentDTO) {

        Department department =  departmentMapper.toEntity(departmentDTO);

        Department savedDepartment = departmentRepository.save(department);

        return departmentMapper.toDto(savedDepartment);
    }

    @Override
    public DepartmentDTO getDepartmentByCode(String departmentCode) {
        Department department = departmentRepository.findByDepartmentCode(departmentCode);

        return departmentMapper.toDto(department);
    }
}

package net.javaguides.departementservice;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@OpenAPIDefinition(
    info = @Info(
        title = "Department Service REST APIs",
        description = "Department Service REST APIs Documentation",
        version = "v1.0",
        contact = @Contact(
            name = "JKA",
            email = "gomeskaze@yahoo.fr",
            url = "https://www.bitech.de"
        ),
        license = @License(
            name = "Apache 2.0",
            url = "https://www.bitech.de/license"
        )
    )
)
@SpringBootApplication
public class DepartementServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(DepartementServiceApplication.class, args);
    }

}

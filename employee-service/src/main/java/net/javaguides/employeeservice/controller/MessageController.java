package net.javaguides.employeeservice.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RefreshScope
@RestController
public class MessageController {

    @Value("${spring.boot.message}") //create a properties file into Git-repo (Config-Server) for employee
    private String message;

    //http://localhost:8084/users/message
    @GetMapping("/users/message")
    public String message() {
        return message;
    }

    /** Achtung::
     * wenn wir etwas auf unserem properties File in Git-repo ändern, dann müssen wir alle Refresh durch das Link:
     * "http://localhost:8084/actuator/refresh" eingeben, um "Message Broker" wie "RabbitMQ" die Änderungen zu updaten.
     * Dann mit GET "http://localhost:8084/users/message" stellt man fest, werden unsere Änderungen erfolgreich
     */
}

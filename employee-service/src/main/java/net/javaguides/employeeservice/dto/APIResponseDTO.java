package net.javaguides.employeeservice.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "APIResponseDTO Model Information")
public class APIResponseDTO {

    private EmployeeDto employee;
    private DepartmentDTO department;
    private OrganizationDto organization;
}

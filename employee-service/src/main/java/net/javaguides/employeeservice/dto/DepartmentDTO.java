package net.javaguides.employeeservice.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "DepartmentDTO Model Information")
public class DepartmentDTO {
    private Long id;
    @Schema(name = "departmentName", description = "Department Name")
    private String departmentName;
    @Schema(name = "departmentDescription", description = "Department Description")
    private String departmentDescription;
    @Schema(name = "departmentCode", description = "Department Code")
    private String departmentCode;
}

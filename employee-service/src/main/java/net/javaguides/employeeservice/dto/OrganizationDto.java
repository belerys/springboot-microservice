package net.javaguides.employeeservice.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "OrganizationDTO Model Information")
public class OrganizationDto {

    private Long id;
    @Schema(name = "organizationName", description = "organization Name")
    private String organizationName;
    @Schema(name = "organizationDescription", description = "organization Description")
    private String organizationDescription;
    @Schema(name = "organizationCode", description = "organization Code")
    private String organizationCode;
    @Schema(name = "createdDate", description = "created Date")
    private LocalDateTime createdDate;
}

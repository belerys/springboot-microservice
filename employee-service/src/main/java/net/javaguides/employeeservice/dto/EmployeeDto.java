package net.javaguides.employeeservice.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "EmployeeDTO Model Information")
public class EmployeeDto {

    private Long id;
    @Schema(name = "firstName", description = "First Name")
    private String firstName;
    @Schema(name = "lastName", description = "Last Name")
    private String lastName;
    @Schema(name = "email", description = "Email")
    private String email;
    @Schema(name = "departmentCode", description = "Department Code")
    private String departmentCode;
    @Schema(name = "organizationCode", description = "Organization Code")
    private String organizationCode;
}

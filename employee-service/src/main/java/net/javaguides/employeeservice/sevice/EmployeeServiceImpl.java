package net.javaguides.employeeservice.sevice;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.retry.annotation.Retry;
import lombok.AllArgsConstructor;
import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;
import net.javaguides.employeeservice.dto.OrganizationDto;
import net.javaguides.employeeservice.exception.ResourceNotFoundException;
import net.javaguides.employeeservice.dto.APIResponseDTO;
import net.javaguides.employeeservice.dto.DepartmentDTO;
import net.javaguides.employeeservice.dto.EmployeeDto;
import net.javaguides.employeeservice.entity.Employee;
import net.javaguides.employeeservice.mapper.EmployeeMapper;
import net.javaguides.employeeservice.repository.EmployeeRepository;
import org.slf4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

@Service
@AllArgsConstructor
@Slf4j
public class EmployeeServiceImpl implements EmployeeService{

    private EmployeeRepository employeeRepository;

    private EmployeeMapper employeeMapper;

    //private RestTemplate restTemplate;
    private WebClient webClient;
    //private APIClient apiClient;

    @Override
    public EmployeeDto saveEmployee(EmployeeDto employeeDto) {

        Employee employee = employeeMapper.toEntity(employeeDto);

        Employee savedEmployee = employeeRepository.save(employee);

        return employeeMapper.toEmployeeDto(savedEmployee);
    }

    //@CircuitBreaker(name = "${spring.application.name}", fallbackMethod = "getDefaultDepartment")
    @Retry(name = "${spring.application.name}", fallbackMethod = "getDefaultDepartment")
    @Override
    public APIResponseDTO findEmployeeById(Long employeeId) {

        LOGGER.info("inside getEmployeeById() method");

        Employee employee = employeeRepository.findById(employeeId)
            .orElseThrow(() -> new ResourceNotFoundException("employee", "employeeId", employeeId));

        EmployeeDto employeeDto = employeeMapper.toEmployeeDto(employee);

        // first Option: restTemplate
        /*ResponseEntity<DepartmentDTO> responseEntity= restTemplate.getForEntity("http://localhost:8080/api/departments/"
            + employee.getDepartmentCode(), DepartmentDTO.class);
        DepartmentDTO departmentDTO = responseEntity.getBody();*/

        //second Option: webClient
        DepartmentDTO departmentDTO = webClient.get()
            .uri("http://localhost:8080/api/departments/" + employee.getDepartmentCode())
            .retrieve()
            .bodyToMono(DepartmentDTO.class)
            .block();

        //third option: FeignClient
        /*DepartmentDTO departmentDTO = apiClient.getDepartmentByCode(employee.getDepartmentCode());*/

        // Call a corresponding organization
        OrganizationDto organizationDto = webClient.get()
            .uri("http://localhost:8083/api/organizations/" + employee.getOrganizationCode())
            .retrieve()
            .bodyToMono(OrganizationDto.class)
            .block();

        // Get employee and his department
        APIResponseDTO apiResponseDTO = new APIResponseDTO();
        apiResponseDTO.setEmployee(employeeDto);
        apiResponseDTO.setDepartment(departmentDTO);
        apiResponseDTO.setOrganization(organizationDto);

        return apiResponseDTO;
    }

    public APIResponseDTO getDefaultDepartment(Long employeeId, Exception exception) {
        Employee employee = employeeRepository.findById(employeeId)
            .orElseThrow(() -> new ResourceNotFoundException("employee", "employeeId", employeeId));

        EmployeeDto employeeDto = employeeMapper.toEmployeeDto(employee);

        // Create Default department
        DepartmentDTO departmentDTO = new DepartmentDTO();
        departmentDTO.setDepartmentName("R&D Department");
        departmentDTO.setDepartmentCode("RD001");
        departmentDTO.setDepartmentDescription("Research and Development Department");

        // Get employee and his department
        APIResponseDTO apiResponseDTO = new APIResponseDTO();
        apiResponseDTO.setEmployee(employeeDto);
        apiResponseDTO.setDepartment(departmentDTO);

        return apiResponseDTO;

    }

}

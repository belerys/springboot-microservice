package net.javaguides.employeeservice;

import feign.codec.Decoder;
import feign.codec.Encoder;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.reactive.function.client.WebClient;

@OpenAPIDefinition(
    info = @Info(
        title = "Employee Service REST APIs",
        description = "Employee Service REST APIs Documentation",
        version = "v1.0",
        contact = @Contact(
            name = "JKA",
            email = "gomeskaze@yahoo.fr",
            url = "https://www.bitech.de"
        ),
        license = @License(
            name = "Apache 2.0",
            url = "https://www.bitech.de/license"
        )
    )
)
@SpringBootApplication
@EnableFeignClients
public class EmployeeServiceApplication {

    //first option
    /*@Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }*/

    //second option
    @Bean
    public WebClient webClient() {
        return WebClient.builder().build();
    }
    // third option
    /*@Bean
    public Decoder decoder() {
        return new JacksonDecoder();
    }

    @Bean
    public Encoder encoder() {
        return new JacksonEncoder();
    }*/

    public static void main(String[] args) {
        SpringApplication.run(EmployeeServiceApplication.class, args);
    }

}

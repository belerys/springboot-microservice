package net.javaguides.employeeservice.mapper;

import java.net.URL;


public class MappingUtils {

    private MappingUtils() {
        // only static methods
    }

    static String mapURL(URL website) {
        return website != null ? website.toString() : null;
    }
}


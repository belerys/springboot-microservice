package net.javaguides.employeeservice.mapper;

import net.javaguides.employeeservice.dto.EmployeeDto;
import net.javaguides.employeeservice.entity.Employee;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.WARN, uses = { MappingUtils.class })
public interface EmployeeMapper {

    EmployeeDto toEmployeeDto(Employee employee);

    Employee toEntity(EmployeeDto employeeDto);
}

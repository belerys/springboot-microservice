package net.javaguides.organizationservice.mapper;

import net.javaguides.organizationservice.dto.OrganizationDto;
import net.javaguides.organizationservice.entity.Organization;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.WARN, uses = { MappingUtils.class })
public interface OrganizationMapper {

    Organization toEntity(OrganizationDto organizationDto);

    OrganizationDto toDto(Organization organization);
}

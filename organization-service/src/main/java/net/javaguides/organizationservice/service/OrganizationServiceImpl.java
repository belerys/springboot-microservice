package net.javaguides.organizationservice.service;

import lombok.AllArgsConstructor;
import net.javaguides.organizationservice.dto.OrganizationDto;
import net.javaguides.organizationservice.entity.Organization;
import net.javaguides.organizationservice.mapper.OrganizationMapper;
import net.javaguides.organizationservice.repository.OrganizationRepository;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class OrganizationServiceImpl implements OrganizationService{

    private OrganizationRepository organizationRepository;

    private OrganizationMapper organizationMapper;

    @Override
    public OrganizationDto saveOrganization(OrganizationDto organizationDto) {
        // convert organizationDto into Organization entity
        Organization organization = organizationMapper.toEntity(organizationDto);

        Organization savedOrganization = organizationRepository.save(organization);

        return organizationMapper.toDto(savedOrganization);
    }

    @Override
    public OrganizationDto getOrganizationByCode(String organizationCode) {

        Organization organization = organizationRepository.findByOrganizationCode(organizationCode);

        return organizationMapper.toDto(organization);
    }

    @Override
    public OrganizationDto findOrganizationById(Long id) {

        return null;
    }
}
